import './App.css';
import Gambar from './Pages/Gambar';
import Stopwarch from './Pages/Stopwarch';

function App() {
  return (
    <div style={{
      width: "100vw",
      display: 'flex',
      flexDirection: 'column',
      justifyContent: "center",
      alignItems: 'center',
      marginTop: "20px"
    }}>
      <div style={{width: "90%", padding: "10px", border:"1px solid black"}} >
      <Stopwarch/>
      </div>
      <div style={{border: "1px solid black", padding: "10px", marginTop: "20px", width: "90%"}} >
        <Gambar/>
      </div>
    </div>
  );
}

export default App;
