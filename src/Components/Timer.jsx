import React from "react";

const Timer = ({ second, minutes, hours, className }) => {
    return (
        <div className={className}>
            <h1 className={className} style={{ textAlign: "center" }} >{second} : {minutes} : {hours}</h1>
            <h1 className={className} style={{ textAlign: "center" }}>Jam : Menit : Detik</h1>
        </div>
    );
};

export default Timer;
