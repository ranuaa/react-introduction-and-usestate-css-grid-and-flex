import React from "react";

const Button = ({ title, className, func }) => {
    return (
        <button className={className} onClick={func}>{title}</button>
    );
};

export default Button;
