import React, { useEffect, useState } from "react";
import Button from "../Components/Button";
import Timer from "../Components/Timer";
import '../Styles/StopWatch.css'



const Stopwarch = () => {
    const [time, setTime] = useState({ hours: 0, minutes: 0, seconds: 0 });
    const [isRunning, setIsRunning] = useState(false);

    const startTimer = () => {
        setIsRunning(true);
    };

    const stopTimer = () => {
        setIsRunning(false);
    };

    const resetTimer = () => {
        setTime({ hours: 0, minutes: 0, seconds: 0 });
        setIsRunning(false);
    };

    useEffect(() => {
        let timer;
        if (isRunning) {
            timer = setInterval(() => {
                setTime(prevTime => {
                    let seconds = prevTime.seconds + 1;
                    let minutes = prevTime.minutes;
                    let hours = prevTime.hours;

                    if (seconds === 60) {
                        seconds = 0;
                        minutes++;
                    }

                    if (minutes === 60) {
                        minutes = 0;
                        hours++;
                    }

                    return { hours, minutes, seconds };
                });
            }, 1000);
        }

        return () => clearInterval(timer);
    }, [isRunning]);

    const formatTime = value => {
        return value.toString().padStart(2, '0');
    };

    return (
        <div className="stopwatch-wrapper" >
            <div className="time-wrapper" >
                <Timer second={formatTime(time.hours)} minutes={formatTime(time.minutes)} hours={formatTime(time.seconds)} />
            </div>
            <div className="button-wrapper">
                <Button title="Reset" className="button-reset" func={resetTimer} />
                <Button title="Start" className="button-start" func={startTimer} />
                <Button title="Stop" className="button-stop" func={stopTimer} />
            </div>
        </div>
    );
};

export default Stopwarch;
