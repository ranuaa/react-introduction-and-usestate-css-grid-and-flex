import React from "react";
import '../Styles/Gambar.css'

const Gambar = () => {
    return (
        <div>
            <div className="atas">
                <div className="wrapper-kiri">
                    <div className="kotak-merah" />
                    <div className="kotak-pink" />
                </div>
                <div className="wrapper-tengah">
                    <div className="segitiga" />
                    <div className="lingkaran" />
                </div>
                <div className="wrapper-kanan">
                    <div className="kotak-biru" />
                </div>
            </div>
            <div className="bawah">
                <div className="kotak-hijau" />
            </div>
        </div>
    );
};

export default Gambar;
